public class OnOffBool {
    public static void main(String args[]) throws Exception {
        boolean bit = false;

        for (int i = 0; i < 20; i += 2) {
            System.out.println("Value of i: " + i + ", value of bit: " + bit);

            bit = (boolean) (bit == false ? true : false);
        }

        System.out.println("");

        for (int i = 0; i < 20; i += 2) {
            System.out.println("Value of i: " + i + ", value of bit: " + bit);

            bit = !bit;
        }
    }
}
