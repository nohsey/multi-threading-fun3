public class StackOverflow {

    public static void main(String args[]) throws Exception {
        MoreNumbers(5);
    }

    public static void MoreNumbers(int number) {
        System.out.println("Current count: " + number);

        MoreNumbers(number++);
    }
}
