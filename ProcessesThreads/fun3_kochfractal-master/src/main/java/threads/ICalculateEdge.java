package threads;

import calculate.Edge;

import java.util.concurrent.ConcurrentHashMap;

public interface ICalculateEdge {
    ConcurrentHashMap<Edge, Edge> returnEdges();
    void addEdge(Edge e);
}
