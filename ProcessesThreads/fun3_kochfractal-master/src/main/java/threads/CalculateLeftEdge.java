package threads;

import calculate.Edge;
import calculate.KochFractal;
import calculate.KochManager;
import javafx.concurrent.Task;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

public class CalculateLeftEdge extends Task implements ICalculateEdge, Runnable {
    private KochFractal koch;
    private KochManager manager;
    private boolean running;

    private ConcurrentHashMap<Edge, Edge> edges;

    public  CalculateLeftEdge(KochManager manager) {
        this.manager = manager;
        this.koch = new KochFractal(this);

        edges = new ConcurrentHashMap<>();
    }

    public void terminateThread(){
        running = false;
    }

    @Override
    public void run() {
        edges.clear();
        running = true;

        while(running) {
            koch.setLevel(manager.getLevel());
            koch.generateLeftEdge();
            terminateThread();
        }


        System.out.println("Ending left edge thread");
    }

    @Override
    public ConcurrentHashMap<Edge, Edge> returnEdges() {
        return edges;
    }

    @Override
    public void addEdge(Edge e) {
        edges.put(e, e);
    }

    @Override
    protected Object call() throws Exception {
        return edges;
    }
}
