/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package calculate;

import java.util.ArrayList;
import java.util.ConcurrentModificationException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.Executors;
import java.util.concurrent.ThreadPoolExecutor;

import fun3kochfractalfx.FUN3KochFractalFX;
import threads.CalculateBottomEdge;
import threads.CalculateLeftEdge;
import threads.CalculateRightEdge;
import timeutil.TimeStamp;

/**
 *
 * @author Nico Kuijpers
 * Modified for FUN3 by Gertjan Schouten
 */
public class KochManager {
    private ConcurrentHashMap<Edge, Edge> edges;
    private FUN3KochFractalFX application;
    private TimeStamp tsCalc;
    private TimeStamp tsDraw;
    private int level = 1;

    private CalculateLeftEdge calculateLeft = new CalculateLeftEdge(this);
    private CalculateRightEdge calculateRight = new CalculateRightEdge(this);
    private CalculateBottomEdge calculateBottom = new CalculateBottomEdge(this);

    private ThreadPoolExecutor executor;

    public int getLevel(){
        return level;
    }

    public KochManager(FUN3KochFractalFX application) {
        this.edges = new ConcurrentHashMap<Edge, Edge>();
        this.application = application;
        this.tsCalc = new TimeStamp();
        this.tsDraw = new TimeStamp();
    }
    
    public void changeLevel(int nxt) {
        calculateLeft.terminateThread();
        calculateRight.terminateThread();
        calculateBottom.terminateThread();

        edges.clear();
        level = nxt;
        tsCalc.init();
        tsCalc.setBegin("Begin calculating");

        //calculateLeft.run();
        //calculateRight.run();
        //calculateBottom.run();

        executor = (ThreadPoolExecutor) Executors.newFixedThreadPool(3);

        executor.execute(calculateLeft);
        executor.execute(calculateRight);
        executor.execute(calculateBottom);

        while (executor.getCompletedTaskCount() < 3) {
            // wait till all threads are done before continuing
        }

        executor.shutdown();

        getAllEdges(calculateLeft.returnEdges());
        getAllEdges(calculateRight.returnEdges());
        getAllEdges(calculateBottom.returnEdges());



        tsCalc.setEnd("End calculating");
        application.setTextNrEdges("" + calculateRight.getKoch().getNrOfEdges());
        application.setTextCalc(tsCalc.toString());
        drawEdges();
    }
    
    public void drawEdges() {
        tsDraw.init();
        tsDraw.setBegin("Begin drawing");
        application.clearKochPanel();

        // Iterator makes it so even if the list has changed meanwhile, it will process it
        Iterator<Edge> edgeIterator = edges.keySet().iterator();
        while(edgeIterator.hasNext()){
            Edge e = edgeIterator.next();
            application.drawEdge(e);
        }

        tsDraw.setEnd("End drawing");
        application.setTextDraw(tsDraw.toString());
    }

    public void addEdge(Edge e) {
        edges.put(e,e);
    }

    private void getAllEdges(ConcurrentHashMap<Edge, Edge> edges){
        Iterator<Edge> edgeIterator = edges.keySet().iterator();
        while(edgeIterator.hasNext()){
            Edge e = edgeIterator.next();
            addEdge(e);
        }
    }
}
